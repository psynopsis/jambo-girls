# Selecting base image Python version 3 for container
FROM python:3

# Set variable PYTHONUNBUFFERED for output STDOUT to terminal
ENV PYTHONUNBUFFERED 1

# Set directory for initialization django
WORKDIR /girls

# Copying repository files and directories to django directory
COPY . /girls/

# Runing install django from file requirements.txt
RUN pip install -r requirements.txt

# Setting volume for saving container data(files)
VOLUME ["/girls/db"]

# Opening network TCP protocol for container - port 80
EXPOSE 80

# Initialization and running Python application django-girls. Take instruction docker CMD for runing container, instruction RUN not running in build process
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:80

